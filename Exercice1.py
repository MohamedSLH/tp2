import json
import csv


with open('data.json', 'r') as json_file:
    complex_numbers = json.load(json_file)


with open('complex_numbers.csv', 'w', newline='') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(['reel', 'imaginaire'])
    for number in complex_numbers:
        writer.writerow(number)