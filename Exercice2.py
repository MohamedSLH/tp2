import csv

def charger_pokemons_csv(fichier):
    pkmn = {}
    with open(fichier, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            nom = row[0]
            stats = [int(stat) for stat in row[1:]]
            pkmn[nom] = stats
    return pkmn

pkmn = charger_pokemons_csv("pokemon.csv")
for nom, stats in pkmn.items():
    print(f"{nom}: {stats}")

print(isinstance(pkmn, dict))
print(isinstance(pkmn["Pikachu"], list))
print(isinstance(pkmn["Pikachu"][0], int))
